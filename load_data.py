import json
import requests
import os
import inspect
import parameters as param

# check if files are in png format
root = os.path.dirname(inspect.getfile(param)) + "/data/samplefiles/"
file_dict = {}
print(root)
for (dirpath, dirnames, filenames) in os.walk(root):
    for file in filenames:
        if '.png' in file:
            # if files are organized in dir, then prepend the dir name to filename
            tmp = {}
            tmp["fullpath"] = dirpath + '/' + file
            file = dirpath + '-' + file
            file = file.replace(root, '')
            file = file.replace('/', '-')
            file = file.replace("\\", '-')
            file_dict[file] = tmp

#pprint.pprint(file_dict)

# post all files
url = 'http://127.0.0.1:8000/ImageFiles'
# first query existing files
response = requests.get(url)
existing_files = response.json()
# mark files for POST or PUT
for file in existing_files:
    if file in file_dict:
        file_dict[file]['update'] = True

# upload files
for file in file_dict:
    if 'update' in file_dict[file]:
        # PUT
        payload = {}
        files = [
            ('file', (file, open(
                file_dict[file]["fullpath"], 'rb'), 'application/octet-stream'))
        ]
        headers = {
            'accept': 'application/json'
        }
        response = requests.request(
            "PUT", url + '/' + file, headers=headers, data=payload, files=files)
        if response.status_code != 200:
            print("update of file {} failed".format(file))
            print("status code is {}".format(str(response.status_code)))
            print("reason is {}".format(response.reason))
        else:
            print("updated file {}".format(file))
    else:
        # POST
        payload = {}
        files = [
            ('file', (file, open(
                file_dict[file]["fullpath"], 'rb'), 'application/octet-stream'))
        ]
        headers = {
            'accept': 'application/json'
        }
        response = requests.request(
            "POST", url, headers=headers, data=payload, files=files)
        if response.status_code != 200:
            print("post of file {} failed".format(file))
            print("status code is {}".format(str(response.status_code)))
            print("reason is {}".format(response.reason))
        else:
            print("posted file {}".format(file))


# read data from sample json and sample files
with open('data/samplejson/question_data.json') as file:
    question_data_list = json.load(file)
# check if data exists
# if yes PUT
# if not POST
url = 'http://127.0.0.1:8000/QuestionData'
response = requests.get(url)
existing_json = response.json()
existing_ids = []
for data in existing_json:
    if data['question_id'] not in existing_ids:
        existing_ids.append(data['question_id'])
for data in question_data_list:
    #print(data)
    if data['question_id'] not in existing_ids:
        response = requests.post(url=url,json=data)
        if response.status_code != 200:
            print("Failed to post QuestionData Document with ID {}".format(data['question_id']))
            print("status code is {}".format(str(response.status_code)))
            print("reason is {}".format(response.reason))
        else:
            print("QuestionData Document with ID {} posted succeessfully".format(data['question_id']))
    else:
        response = requests.put(url=url + '/' + data['question_id'], json=data)
        if response.status_code != 200:
            print("Failed to update QuestionData Document with ID {}".format(data['question_id']))
            print("status code is {}".format(str(response.status_code)))
            print("reason is {}".format(response.reason))
        else:
            print("QuestionData Document with ID {} updated succeessfully".format(data['question_id']))

with open('data/samplejson/student_data.json') as file:
    student_data_list = json.load(file)
url = 'http://127.0.0.1:8000/StudentData'
response = requests.get(url)
existing_json = response.json()
existing_ids = []
for data in existing_json:
    if data['student_id'] not in existing_ids:
        existing_ids.append(data['student_id'])
for data in student_data_list:
    #print(data)
    if data['student_id'] not in existing_ids:
        response = requests.post(url=url,json=data)
        if response.status_code != 200:
            print("Failed to post StudentData Document with ID {}".format(data['student_id']))
            print("status code is {}".format(str(response.status_code)))
            print("reason is {}".format(response.reason))
        else:
            print("StudentData Document with ID {} posted succeessfully".format(data['student_id']))
    else:
        response = requests.put(url=url + '/' + data['student_id'], json=data)
        if response.status_code != 200:
            print("Failed to update StudentData Document with ID {}".format(data['student_id']))
            print("status code is {}".format(str(response.status_code)))
            print("reason is {}".format(response.reason))
        else:
            print("StudentData Document with ID {} updated succeessfully".format(data['student_id']))

with open('data/samplejson/question_meta.json') as file:
    question_meta_list = json.load(file)
url = 'http://127.0.0.1:8000/QuestionMeta'
response = requests.get(url)
existing_json = response.json()
existing_ids = []
for data in existing_json:
    if data['question_id'] not in existing_ids:
        existing_ids.append(data['question_id'])
for data in question_meta_list:
    #print(data)
    if data['question_id'] not in existing_ids:
        response = requests.post(url=url,json=data)
        if response.status_code != 200:
            print("Failed to post QuestionMeta Document with ID {}".format(data['question_id']))
            print("status code is {}".format(str(response.status_code)))
            print("reason is {}".format(response.reason))
        else:
            print("QuestionMeta Document with ID {} posted succeessfully".format(data['question_id']))
    else:
        response = requests.put(url=url + '/' + data['question_id'], json=data)
        if response.status_code != 200:
            print("Failed to update QuestionMeta Document with ID {}".format(data['question_id']))
            print("status code is {}".format(str(response.status_code)))
            print("reason is {}".format(response.reason))
        else:
            print("QuestionMeta Document with ID {} updated succeessfully".format(data['question_id']))

with open('data/samplejson/student_meta.json') as file:
    student_meta_list = json.load(file)
url = 'http://127.0.0.1:8000/StudentMeta'
response = requests.get(url)
existing_json = response.json()
existing_ids = []
for data in existing_json:
    if data['student_id'] not in existing_ids:
        existing_ids.append(data['student_id'])
for data in student_meta_list:
    if data['student_id'] not in existing_ids:
        response = requests.post(url=url,json=data)
        if response.status_code != 200:
            print("Failed to post StudentMeta Document with ID {}".format(data['student_id']))
            print("status code is {}".format(str(response.status_code)))
            print("reason is {}".format(response.reason))
        else:
            print("StudentMeta Document with ID {} posted succeessfully".format(data['student_id']))
    else:
        response = requests.put(url=url + '/' + data['student_id'], json=data)
        if response.status_code != 200:
            print("Failed to update StudentMeta Document with ID {}".format(data['student_id']))
            print("status code is {}".format(str(response.status_code)))
            print("reason is {}".format(response.reason))
        else:
            print("StudentMeta Document with ID {} updated succeessfully".format(data['student_id']))
