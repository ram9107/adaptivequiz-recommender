# adaptivequiz-recommender

## Description
Project used to deploy adaptivequiz restapi server and run tests against it.

## Installation
After cloning this repo, run this command inside the project directory:

<code>docker compose up -d
</code>

Installtion python libraries:

<code>pip install --no-cache-dir -r requirements.txt
</code>

To stop server, run these commands inside the project directory:

<code>docker compose down
</code>

To remove saved data:

<code>docker volume rm restapp_dbvolume01 restapp_filevolume01
</code>

## Usage
In order to load data into the server, you need to copy files and data in json format to /data folder. Please refer examples shown. After customizing the folder, run this python command

<code>python load_data.py
</code>

To clean up all data from server, run:

<code>python remove_data.py
</code>


To run test, update <code>serveQuestion</code> function in file <code>serve_question.py</code> and run:

<code>python serve_question.py
</code>

## Support
TBD

## Roadmap
TDB

## Contributing
TBD

## Authors and acknowledgment
TBD

## License
TBD

## Project status
TDB
