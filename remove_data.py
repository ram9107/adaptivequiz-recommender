import requests


url = 'http://127.0.0.1:8000/QuestionData'
response = requests.get(url)
existing_json = response.json()
for data in existing_json:
    response = requests.delete(url + '/' + data['question_id'])
    if response.status_code != 200:
        print("Unable to delete QuestionData Document with ID {}".format(data['question_id']))
        print("status code is {}".format(str(response.status_code)))
        print("reason is {}".format(response.reason))
    else:
        print("QuestionData Document with ID {} deleted successfully".format(data['question_id']))

url = 'http://127.0.0.1:8000/StudentData'
response = requests.get(url)
existing_json = response.json()
for data in existing_json:
    response = requests.delete(url + '/' + data['student_id'])
    if response.status_code != 200:
        print("Unable to delete StudentData Document with ID {}".format(data['student_id']))
        print("status code is {}".format(str(response.status_code)))
        print("reason is {}".format(response.reason))
    else:
        print("StudentData Document with ID {} deleted successfully".format(data['student_id']))

url = 'http://127.0.0.1:8000/QuestionMeta'
response = requests.get(url)
existing_json = response.json()
for data in existing_json:
    response = requests.delete(url + '/' + data['question_id'])
    if response.status_code != 200:
        print("Unable to delete QuestionMeta Document with ID {}".format(data['question_id']))
        print("status code is {}".format(str(response.status_code)))
        print("reason is {}".format(response.reason))
    else:
        print("QuestionMeta Document with ID {} deleted successfully".format(data['question_id']))

url = 'http://127.0.0.1:8000/StudentMeta'
response = requests.get(url)
existing_json = response.json()
for data in existing_json:
    response = requests.delete(url + '/' + data['student_id'])
    if response.status_code != 200:
        print("Unable to delete StudentMeta Document with ID {}".format(data['student_id']))
        print("status code is {}".format(str(response.status_code)))
        print("reason is {}".format(response.reason))
    else:
        print("StudentMeta Document with ID {} deleted successfully".format(data['student_id']))


url = 'http://127.0.0.1:8000/ImageFiles'
# first query existing files
response = requests.get(url)
existing_files = response.json()
# mark files for POST or PUT
for file in existing_files:
    response = requests.delete(url + '/' + file)
    if response.status_code != 200:
        print("Unable to delete File {}".format(file))
        print("status code is {}".format(str(response.status_code)))
        print("reason is {}".format(response.reason))
    else:
        print("File {} deleted successfully".format(file))